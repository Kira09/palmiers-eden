import './App.scss'
import { useState } from "react";

import {
  MaxwidthWrapper,
  Navbar,
  SeachFood,
  ScroolMouse,
  MenuDuJourCard,
  WhatsappBtn,
  Espace,
  ToutMenu
} from './components'
import { MdRestaurantMenu } from "react-icons/md";
import { MdTableBar } from "react-icons/md";

import { IoFastFoodOutline } from "react-icons/io5";

import image1 from '/assets/img/food/3.png'

import {menuListe, MENU} from "./utils/db/menu.js";
import { FilterContext } from "./utils/context/FilterContext.jsx";


import { gsap } from "gsap";
import { useGSAP } from '@gsap/react'

function App() {
  const [value, setValue] = useState('')
  const [typeFood, setTypeFood] = useState('entrees')
  const getTypeFood = (event) => {
    setTypeFood(event.target.value)
  }

  return (
      <FilterContext.Provider value={{value, setValue}}>
        <div className='landigng'>
        {/*<img src="/assets/img/bg/wallpapper.jpg" alt="" className={'bg-image'}/>*/}
        <MaxwidthWrapper>
          <Navbar/>
          <div className='landing-hero'>
            <div className="landing-hero_rigth">
              <h1> <strong>Vous avez faim ? </strong><br /> Alors vous êtes au bon endroit. </h1>
              <p>Nos délicieuses nourritures vous attendent. Nous sommes toujours près de vous avec des produits frais.</p>
              <div>
                <button>Voir le menu</button>
              </div>
            </div>
            <div className={'img-container'}>
              <img src={image1} alt="" height={400} width={400}/>
            </div>

          </div>

          <div className={'menu-jour'}>
            <h2><MdRestaurantMenu size={24} className={'logo-menu_jour'}/> <span>Menu du jour</span></h2>
            <div className="menu-jour_cards">
              {
                menuListe.map((menu) => {
                  return (
                      <MenuDuJourCard key={menu.title} {...menu}/>
                  )
                })
              }
            </div>
          </div>
          <div className={'all-menu'}>
            <div className={'all-menu_header'}>
              <div>
                <h2><span>Menu</span></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br/> A beatae blanditiis dignissimos ea,</p>
              </div>
              <div className={'menu-types_container'}>
                <SeachFood/>
                <div className={'menu-type'}>
                  <input type="radio" name={'type'} value={'all'} id={'all'} onChange={getTypeFood}/>
                  <label htmlFor={'all'}><IoFastFoodOutline/> Tout</label>
                </div>
                <div className={'menu-type'}>
                  <input type="radio" name={'type'} value={'entrees'} id={'entree'} onChange={getTypeFood}/>
                  <label htmlFor={'entree'}>Entrée</label>
                </div>
                <div className={'menu-type'}>
                  <input type="radio" name={'type'} value={'resistances'} id={'resistance'} onChange={getTypeFood}/>
                  <label htmlFor={'resistance'}>Résistance</label>
                </div>
                <div className={'menu-type'}>
                  <input type="radio" name={'type'} value={'africains'} id={'africain'} onChange={getTypeFood}/>
                  <label htmlFor={'africain'}>Africain</label>
                </div>
                <div className={'menu-type'}>
                  <input type="radio" name={'type'} value={'garnitures'} id={'garniture'} onChange={getTypeFood}/>
                  <label htmlFor={'garniture'}>Garniture</label>
                </div>
                <div className={'menu-type'}>
                  <input type="radio" name={'type'} value={'desserts'} id={'dessert'} onChange={getTypeFood}/>
                  <label htmlFor={'dessert'}>Dessert</label>
                </div>
                <div className={'menu-type'}>
                  <input type="radio" name={'type'} value={'boissons'} id={'boisson'} onChange={getTypeFood}/>
                  <label htmlFor={'boisson'}>Boisson</label>
                </div>
              </div>
            </div>
            <ToutMenu type={typeFood}  />
            <div className={'event-container'}></div>
          </div>


        </MaxwidthWrapper>

        <Espace/>
        <ScroolMouse/>
        <WhatsappBtn/>
      </div>
      </FilterContext.Provider>
  )
}

export default App
