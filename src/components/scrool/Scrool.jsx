import { BsMouse } from "react-icons/bs";
import './scroll.scss'
export const ScroolMouse = () =>{
    return(
        <div className={'scroll-container'}>
            <div className={'scroll'}>

                <BsMouse size={36}/>
                <span>scrollez </span>
            </div>
        </div>
    )
}