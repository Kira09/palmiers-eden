import './socials.scss'

import whatsapp from '/assets/img/logo/WhatsApp_icon.png.webp'
import facebook from '/assets/img/logo/Facebook.png'
export const WhatsappBtn = () =>{
    return(
        <div className={'socials'}>
            < a href={'https://web.facebook.com/lespalmiersdeden'} target={'_blank'} className={'whatsapp-logo'} >
                <img src={facebook} alt="" width={30} height={30}/>
            </a>
            < a href={"https://wa.me/+2250768586639"} target={'_blank'} className={'whatsapp-logo'} >
                <img src={whatsapp} alt="" width={40} height={40}/>
            </a>
        </div>
    )
}