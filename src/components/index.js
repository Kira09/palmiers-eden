import { MaxwidthWrapper } from "./wrapper/MaxwidthWrapper";
import { Navbar } from "./navbar/Navbar.jsx";
import { SeachFood } from "./search-food/SeachFood.jsx";
import { ScroolMouse} from "./scrool/Scrool.jsx";
import { MenuDuJourCard } from "./menu/menu-jour/MenuDuJourCard.jsx";
import { ToutMenu } from "./menu/tout-menu/tout-menu.jsx";
import { WhatsappBtn } from "./social-networks/whatsapp/WhatsappBtn.jsx";
import { Espace} from "./espace/Espace.jsx";

export { MaxwidthWrapper, Navbar, SeachFood, ScroolMouse, MenuDuJourCard, ToutMenu, WhatsappBtn, Espace }