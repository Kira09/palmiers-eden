import React from 'react'
import './widh-style.scss'

export const MaxwidthWrapper = ({children}) => {
  return (
    <div className='maxwidth-wrapper'>
        {children}
    </div>
  )
}
