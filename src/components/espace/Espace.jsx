import {MaxwidthWrapper} from "../wrapper/MaxwidthWrapper.jsx";
import './espace.scss'

import salle1 from '/assets/img/espace/salle1.jpg'
export const Espace = ()=>{
    return(
        <div className={'espace-container'} >
            <MaxwidthWrapper>
                <div className={'espace-salle'}>
                    <h2>Espace <span>&</span><br/>Salle événementielle</h2>

                    <div className={'espace'}>
                        <div className={'espace-card'}>
                            <div className={'espace-img_container'}>
                                <img src={salle1} alt=""/>
                            </div>

                            <div className={'espace-card_description'}>
                                <h1>Salle des événements</h1>
                            </div>
                        </div>
                    </div>

                </div>
            </MaxwidthWrapper>
        </div>
    )
}