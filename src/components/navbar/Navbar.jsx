import './nav.scss'
import { TbGridDots } from "react-icons/tb";

import logo from '/assets/img/logo/palmier-logo.svg'

export const Navbar = () =>{
    return(
        <nav className={'nav-container'}>
            <img src={logo} alt="" width={150}/>

            {/*<ul>
                <li>Acceuil</li>
                <li>Menu</li>
                <li>A Propos</li>
            </ul>*/}
            <TbGridDots size={36}/>
        </nav>
    )
}