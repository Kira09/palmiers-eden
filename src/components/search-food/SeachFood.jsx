import './search.scss'
import { CiSearch } from "react-icons/ci";
import {useContext, useState} from "react";
import {FilterContext} from "../../utils/context/FilterContext.jsx";

export const SeachFood = () => {

    const { setValue } = useContext(FilterContext)
    const onValueChange = (event) =>{
        setValue(event.target.value)
    }
    return (
        <div className={'search'}>
            <input type="text" placeholder={'Recherchez un plat'} onChange={onValueChange}/>
            <span><CiSearch size={24}/></span>
        </div>
    );
}
