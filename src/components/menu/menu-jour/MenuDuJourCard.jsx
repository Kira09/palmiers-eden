import './menu-jour.scss'

export const MenuDuJourCard = ({title, price, imgUrl, description}) =>{
    return(
        <div className={'menu-jour-card'}>
            <div className={'menu-jour-card_img__container'}>
                <img src={imgUrl} alt="" width={110} height={110}/>
            </div>
            <div className="description">
                <h3>{title}</h3>
                <p>{description}</p>

                <strong>{price}f cfa </strong>
            </div>
        </div>
    )
}