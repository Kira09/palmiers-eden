import {useState, useEffect, useContext} from "react";
import './all-menu.scss';

import { menuListe2, MENU} from "../../../utils/db/menu.js";
import {FilterContext} from "../../../utils/context/FilterContext.jsx";

export const ToutMenu = ({type}) => {

    const {value} = useContext(FilterContext)

    const [menu, setMenu] = useState(menuListe2)
    console.log('ALL MENU FILTER', value)

    useEffect(() => {
        setMenu(MENU[type])
    }, [MENU[type]]);


    return (
        <ul className={'all-menu_content'}>
            <div className={'all-menu_liste'}>
                {

                    menu.map((m) => {
                        if (m.title.toLowerCase().includes(value)){
                            return (
                                <li key={m.title} className={'menu-item'}>
                                    <div className={'title'}>
                                        <img src={m.imgUrl} alt="" width={80} height={80}/>
                                    </div>
                                    <div className={'description'}>
                                        <h3>{m.title}</h3>
                                        <p>{m.description}</p>
                                    </div>
                                    <div className={'price'}>
                                        <strong>{m.price} F CFA</strong>
                                    </div>
                                </li>
                            )
                        }
                    })
                }
            </div>
        </ul>
    )
}