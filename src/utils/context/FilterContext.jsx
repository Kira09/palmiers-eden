import {createContext} from "react";

export const FilterContext = createContext({
    value: '',
    setValue: () => {}
})
