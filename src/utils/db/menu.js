const menuListe = [
    {title:'Agouti', price:3500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
    {title:'Lapin', price:3500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
    {title:'Sosso', price:3500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
    {title:'Machoiron', price:3500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
]

const menuListe2 = [
    {title:'Agouti', price:3500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
    {title:'Lapin', price:3500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
    {title:'Sosso', price:3500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
    {title:'Machoiron', price:3500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
    {title:'pintade', price:3500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
    {title:'Araie', price:3500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
    {title:'Somon', price:3500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
    {title:'Bar', price:3500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
]


const MENU = {
    entrees : [
        {title:'Salade Suisse', price:3000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Salade mixte au thon', price:3000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Delice du jardin', price:2500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Salade cesar', price:3500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Crepe fourree aux jambons', price:3000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Carotte rapee au thon', price:2500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Avocat au crevette', price:3000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Salade parisienne', price:3500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Salade Suisse au saumon et avocat', price:3500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
    ],
    resistances : [
        {title:'Entrecote de veau', price:8000, imgUrl:"/assets/img/food/3.png", description:"sauce au poivre vert, du riz"},
        {title:'Roti de porc à la bière', price:6000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Brochette de fillet de boeuf', price:6000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Darne de bar mariné', price:6000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Cuisse de poulet à l\'Italienne', price:5000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Lapin chasseur', price:16000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Brochette de gésier', price:5000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Escargot sauté', price:8000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Sole au citron et persil', price:8000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Emincé de blanc de poulet', price:10000, imgUrl:"/assets/img/food/3.png", description:"Sauce persto / Tagliatelle"},
        {title:'Panné aux crevettes', price:10000, imgUrl:"/assets/img/food/3.png", description:"Sauce persto / Tagliatelle"},
        {title:'Farfalle à la parisienne', price:10000, imgUrl:"/assets/img/food/3.png", description:"Sauce persto / Tagliatelle"},
        {title:'Poulet à la crème et champion', price:10000, imgUrl:"/assets/img/food/3.png", description:"Sauce persto / Tagliatelle"},
        {title:'Poulet à l\'Ivoirienne', price:8000, imgUrl:"/assets/img/food/3.png", description:"Sauce persto / Tagliatelle"},
        {title:'Poulet sauté l\'ail et persil', price:8000, imgUrl:"/assets/img/food/3.png", description:"Sauce persto / Tagliatelle"},
        {title:'Lapin à la moutarde', price:15000, imgUrl:"/assets/img/food/3.png", description:"Sauce persto / Tagliatelle"},
    ],
    africains : [
        {title:'Kédjénou de poulet', price:8000, imgUrl:"/assets/img/food/3.png", description:"sauce au poivre vert, du riz"},
        {title:'Kédjénou d\'agouti', price:15000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Kédjénou de pintade', price:15000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Kédjénou de pondeuse', price:13000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Soupe d\'araie', price:15000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Soupe de bar', price:20000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Soupe de brochet', price:5000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Soupe de machoiron', price:8000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Soupe de capitaine', price:8000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Soupe du pêcheur', price:25000, imgUrl:"/assets/img/food/3.png", description:"Sauce persto / Tagliatelle"},
        {title:'Pêpê soupe de mouton', price:10000, imgUrl:"/assets/img/food/3.png", description:"Sauce persto / Tagliatelle"},
    ],
    garnitures : [
        {title:'Riz nature', price:3000, imgUrl:"/assets/img/food/3.png", description:"sauce au poivre vert, du riz"},
        {title:'Alloco', price:3000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Frites d\'ignames', price:2500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Pomme vapeur', price:3000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Frites de pomme de terre', price:2500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Pomme paysanne', price:3000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Riz au lardons et poivron', price:5000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
    ],
    desserts : [
        {title:'Salade de fruits', price:2500, imgUrl:"/assets/img/food/3.png", description:"sauce au poivre vert, du riz"},
        {title:'Ananas flambe', price:3000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Crepe au chocolat', price:3000, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Assiette de fruits', price:2500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
        {title:'Coupe de fruits à la chantilly', price:2500, imgUrl:"/assets/img/food/3.png", description:"1 bol de riz"},
    ]
}

export { menuListe, menuListe2, MENU}

